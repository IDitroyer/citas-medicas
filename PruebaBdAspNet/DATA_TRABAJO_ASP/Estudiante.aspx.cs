﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace PruebaBdAspNet
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        DBimplemetation impl = new DBimplemetation();
        string html = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            this.ListarEstudiantes();
        }

        private void ListarEstudiantes()
        {
            if (impl.Consultar().Rows.Count > 0)
            {
                html += "<table class='table table-sm table-hover table-striped table-bordered'>";
                html += "<thead><tr><td>ID</td><td>Nombre</td><td>Apellido</td></tr></thead>";
                html += "<tbody>";
                foreach (DataRow dbRow in impl.Consultar().Rows)
                {
                    html += "<tr>";
                    html += "<td>" + dbRow["id"].ToString() + "</td>";
                    html += "<td>" + dbRow["nombre"].ToString() + "</td>";
                    html += "<td>" + dbRow["apellido"].ToString() + "</td>";
                    html += "</tr>";
                }
                html += "</tbody>";
                html += "</table>";
            }
            else
            {
                html += "<table class='table table-striped table-bordered'>";
                html += "<tr><td>No hay registros</td></tr>";
                html += "</table>";
            }
            Literal1.Text = html;
            CmbRegistro.DataSource = impl.Consultar();
            CmbRegistro.DataTextField = "nombre";
            CmbRegistro.DataValueField = "nombre";
            CmbRegistro.DataBind();

            CmbEditar.DataSource = impl.Consultar();
            CmbEditar.DataTextField = "nombre";
            CmbEditar.DataValueField = "nombre";
            CmbEditar.DataBind();
        }

        protected void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (TxtID.Text != "" & TxtNombre.Text != "" & TxtApellio.Text != "")
            {
                if (impl.Insertar(TxtID.Text, TxtNombre.Text, TxtApellio.Text))
                {
                    LblGuardar.Text = "El registro se guardo correctamente";
                    TxtID.Text = "";
                    TxtApellio.Text = "";
                    TxtNombre.Text = "";
                    html = "";
                    this.ListarEstudiantes();
                }
                else
                {
                    LblGuardar.Text = "El registro no se pudo guardar";
                }
            }
            else
            {
                LblGuardar.Text = "Por favor completa los campos";
            }
        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (CmbRegistro.Text != "")
            {
                if (impl.Eliminar( CmbRegistro.Text.Trim() ) )
                {
                    LblEliminar.Text = "El registro se elimino correctamente";
                    CmbRegistro.DataSource = impl.Consultar();
                    CmbRegistro.DataTextField = "nombre";
                    CmbRegistro.DataValueField = "nombre";
                    CmbRegistro.DataBind();

                    html = "";
                    this.ListarEstudiantes();
                }
                else
                {
                    LblEliminar.Text = "El registro no se pudo eliminar";
                }
            }
        }


        protected void BtnSelectEdit_Click(object sender, EventArgs e)
        {
            if (CmbEditar.Text != "")
            {

                LblEditar.Text = "Estudiante seleccionado";

                TextIdEdit.Text = CmbEditar.DataValueField;
                TextNombreEdit.Text = CmbEditar.DataTextField;
                TextApellidoEdit.Text = CmbEditar.DataValueField;


                foreach (DataRow dbRow in impl.Consultar().Rows)
                {
                    if ( dbRow["nombre"].ToString().Trim() == CmbEditar.Text.Trim() )
                    {
                        TextIdEdit.Text = dbRow["id"].ToString();
                        TextNombreEdit.Text = dbRow["nombre"].ToString();
                        TextApellidoEdit.Text = dbRow["apellido"].ToString();
                    }
                }

            }
        }


        protected void BtnEditar_Click(object sender, EventArgs e)
        {
            if (TextIdEdit.Text != "" & TextNombreEdit.Text != "" & TextApellidoEdit.Text != "")
            {
                if (impl.Editar(TextIdEdit.Text, TextNombreEdit.Text, TextApellidoEdit.Text))
                {
                    LblEditar2.Text = "El registro se guardo correctamente";
                    TextIdEdit.Text = "";
                    TextNombreEdit.Text = "";
                    TextApellidoEdit.Text = "";
                    html = "";
                    this.ListarEstudiantes();
                }
                else
                {
                    LblEditar2.Text = "El registro no se pudo guardar";
                }
            }
            else
            {
                LblEditar2.Text = "Por favor completa los campos";
            }
        }
    }
}