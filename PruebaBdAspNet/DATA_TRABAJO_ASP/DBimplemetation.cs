﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace PruebaBdAspNet
{
    public class DBimplemetation
    {
        bool ejecuto;
        DBconection bd = new DBconection();

        public DataTable Consultar()
        {
            DataTable dt;
            bd.Conectar();
            bd.CrearComando("SELECT id, nombre, apellido FROM estudiante");
            dt = bd.EjecutarDataTable();
            bd.Desconectar();
            return dt;
        }

        public bool Insertar(string id, string nombre, string apellido)
        {
            bd.Conectar();
            bd.CrearComando("INSERT INTO estudiante(id, nombre, apellido) VALUES(@id,@nombre,@apellido) ");
            bd.AsignarParametro("@id", SqlDbType.VarChar, id);
            bd.AsignarParametro("@nombre", SqlDbType.VarChar, nombre);
            bd.AsignarParametro("@apellido", SqlDbType.VarChar, apellido);
            if (bd.EjecutarConsulta() > 0)
            {
                ejecuto = true;
            }
            else
            {
                ejecuto = false;
            }
            return ejecuto;
        }

        public bool Eliminar(string nombre)
        {
            bd.Conectar();
            bd.CrearComando("DELETE FROM estudiante WHERE nombre = @nombre ");
            bd.AsignarParametro("@nombre", SqlDbType.VarChar, nombre);
            if (bd.EjecutarConsulta() > 0)
            {
                ejecuto = true;
            }
            else
            {
                ejecuto = false;
            }
            return ejecuto;
        }

        public bool Editar(string id, string nombre, string apellido)
        {
            bd.Conectar();
            bd.CrearComando("UPDATE estudiante SET id = @id, nombre = @nombre, apellido = @apellido WHERE id = @id ");
            bd.AsignarParametro("@id", SqlDbType.VarChar, id);
            bd.AsignarParametro("@nombre", SqlDbType.VarChar, nombre);
            bd.AsignarParametro("@apellido", SqlDbType.VarChar, apellido);
            if (bd.EjecutarConsulta() > 0)
            {
                ejecuto = true;
            }
            else
            {
                ejecuto = false;
            }
            return ejecuto;
        }
    }
}