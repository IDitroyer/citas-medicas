﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Estudiante.aspx.cs" Inherits="PruebaBdAspNet.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CRUD en ASP.NET y SQl Server</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
        <div class="jumbotron" style="padding:20px;text-align:center">
            <h3>CRUD en ASP.NET SQl Server</h3>
        </div>

        <div class="row">
            

            <div class="row">
                <div class="col-md-6" style="text-align:center">
                <div class="well">
                    <h4 class="text-success">Insertar nuevo estudiante</h4><hr />

                    <p><i class="glyphicon glyphicon-phone"></i> ID:</p>
                    <p><asp:TextBox ID="TxtID" runat="server" style="margin:0 auto" CssClass="form-control" Width="250px"></asp:TextBox></p>
                    <p><asp:Label ID="LblGuardar" runat="server"></asp:Label></p>
                    <p><i class="glyphicon glyphicon-user"></i> Nombre:</p>
                    <p><asp:TextBox ID="TxtNombre" runat="server" style="margin:0 auto" CssClass="form-control" Width="250px"></asp:TextBox></p>
                    <p><i class="glyphicon glyphicon-home"></i> Apellido:</p>
                    <p><asp:TextBox ID="TxtApellio" runat="server" style="margin:0 auto" CssClass="form-control" Width="250px"></asp:TextBox></p>
                    <p><asp:Button ID="BtnGuardar" runat="server" CssClass="btn btn-success" Text="Guardar Estudiante" OnClick="BtnGuardar_Click" /></p>
                </div>
            </div>
            <div class="col-md-6" style="text-align:center">
                <h4>Consulta desde la BD al cargar la página</h4><hr />
                <asp:Literal ID="Literal1" runat="server"></asp:Literal>
            </div>
            </div>


            <div class="row">

             <div class="col-md-12">
                <hr />
             </div>




             <div class="col-md-6" style="text-align:center">
                <h4 class="text-danger">Escoge el estudiante que desees eliminar</h4><hr />
                <p><asp:Label ID="LblEliminar" runat="server"></asp:Label></p>
                <p><asp:DropDownList ID="CmbRegistro" CssClass="form-control" runat="server"></asp:DropDownList></p>
                <p><asp:Button ID="BtnEliminar" runat="server" CssClass="btn btn-primary" Text="Eliminar Estudiante" OnClick="BtnEliminar_Click" /></p>
            </div>





            <div class="col-md-6" style="text-align:center">

                <h4 class="text-info" >Escoge el estudiante que desees editar</h4><hr />
                <p><asp:Label ID="LblEditar" runat="server"></asp:Label></p>
                <p><asp:DropDownList ID="CmbEditar" CssClass="form-control" runat="server" ></asp:DropDownList></p>
                <p><asp:Button ID="BtnEdit" runat="server" CssClass="btn btn-primary" Text="Seleccionar Estudiante" OnClick="BtnSelectEdit_Click" /></p>

                <hr />

                <div class="well">
                    <h4 class="text-info">Editar estudiante</h4><hr />

                    <p><i class="glyphicon glyphicon-phone"></i> ID:</p>
                    <p><asp:TextBox ID="TextIdEdit" runat="server" style="margin:0 auto" CssClass="form-control" Width="250px"></asp:TextBox></p>
                    <p><asp:Label ID="LblEditar2" runat="server"></asp:Label></p>
                    <p><i class="glyphicon glyphicon-user"></i> Nombre:</p>
                    <p><asp:TextBox ID="TextNombreEdit" runat="server" style="margin:0 auto" CssClass="form-control" Width="250px"></asp:TextBox></p>
                    <p><i class="glyphicon glyphicon-home"></i> Apellido:</p>
                    <p><asp:TextBox ID="TextApellidoEdit" runat="server" style="margin:0 auto" CssClass="form-control" Width="250px"></asp:TextBox></p>
                    <p><asp:Button ID="BtnEditar" runat="server" CssClass="btn btn-primary" Text="Editar Estudiante" OnClick="BtnEditar_Click" /></p>
                </div>
            </div>
            </div>


        </div>
    </div>
    </form>
</body>
</html>