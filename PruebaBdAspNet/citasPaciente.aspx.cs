﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Dao;
using Business;

namespace PruebaBdAspNet
{
    public partial class citasPaciente : System.Web.UI.Page
    {
        public Boolean alert = false;
        CitaDao citaDao = new CitaDao();
        UsuarioDao usuarioDao = new UsuarioDao();   
        public List<Cita> citasList = new List<Cita>();
        public Usuario usuario = new Usuario();
        public List<Usuario> medicosList = new List<Usuario>();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Auth();
        }

        protected void Auth()
        {
            if (Session["idUsuario"] == null)
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                this.Consultar();
                this.ConsultarMedicos();
            }
        }

        private void Consultar()
        {
            usuario = usuarioDao.findById(int.Parse(Session["idUsuario"].ToString()));
            citasList = citaDao.findCitasByUsuario(usuario.id, 0);

        }

        private void ConsultarMedicos()
        {
            medicosList = usuarioDao.findAllMedicos();

            if (medicosList.Count > 0)
            {
                selectMedicoValue.Enabled = true;
                selectMedicoValue.DataTextField = "nombres";
                selectMedicoValue.DataValueField = "id";
                selectMedicoValue.DataSource = medicosList;
                selectMedicoValue.DataBind();
            }
            else
            {
                selectMedicoValue.Enabled = false;

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string vDate = (inputDate.Value).ToString();
            string vMedicoId = (selectMedicoValue.SelectedValue);
            string vHora = (hourValue.SelectedValue).ToString();
            System.Diagnostics.Debug.WriteLine("Debug message: " + vDate);
            System.Diagnostics.Debug.WriteLine("Debug message: " + int.Parse(vMedicoId));
            System.Diagnostics.Debug.WriteLine("Debug message: " + vHora);
            System.Diagnostics.Debug.WriteLine("Lugar: CUA Enfermeria");
            if (citaDao.Insertar(int.Parse(Session["idUsuario"].ToString()), int.Parse(vMedicoId), "CUA Enfermeria", "1", vHora, vDate.ToString()))
            {
                this.Consultar();
                this.limpiar();
                alert = true;
            }
            else
            {
                this.limpiar();
                alert = false;
            }
        }

        private void limpiar()
        {
            inputDate.Value = "";
            selectMedicoValue.SelectedIndex = 0;
            hourValue.SelectedIndex = 0;
        }

        protected void logOut(object sender, EventArgs e)
        {
            Session["idUsuario"] = null;
            Response.Redirect("login.aspx");
        }

    }
}