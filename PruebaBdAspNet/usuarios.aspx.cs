﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Dao;
using Business;

namespace PruebaBdAspNet
{
    public partial class usuarios : System.Web.UI.Page
    {

        UsuarioDao userDao = new UsuarioDao();
        public Usuario usuario = new Usuario();
        public List<Usuario> usuariosList = new List<Usuario>(); 

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Auth();
        }


        protected void Auth()
        {
            if (Session["idUsuario"] == null)
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                this.Consultar();
                this.ConsultarUser();
            }
        }

        private void Consultar()
        {
            usuariosList = userDao.findAll();
            //LiteralUsuarios.Text = html;
        }

        private void ConsultarUser()
        {
            usuario = userDao.findById(int.Parse(Session["idUsuario"].ToString()));

            //LiteralUsuarios.Text = html;
        }

        protected void logOut(object sender, EventArgs e)
        {
            Session["idUsuario"] = null;
            Response.Redirect("login.aspx");
        }
    }
}
