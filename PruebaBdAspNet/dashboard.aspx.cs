﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Dao;
using Business;

namespace PruebaBdAspNet
{
    public partial class dashboard : System.Web.UI.Page
    {
        UsuarioDao usuarioDao = new UsuarioDao();
        public Usuario usuario = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Auth();
        }

        private void Consultar()
        {
            usuario = usuarioDao.findById(int.Parse(Session["idUsuario"].ToString()));

            //LiteralUsuarios.Text = html;
        }

        protected void Auth()
        {
            if (Session["idUsuario"] == null)
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                this.Consultar();
            }
        }

        protected void logOut(object sender, EventArgs e)
        {
            Session["idUsuario"] = null;
            Response.Redirect("login.aspx");
        }
    }
}