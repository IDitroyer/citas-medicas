﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="PruebaBdAspNet.login" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Americaclinic - Login</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>
    
<style>
    #card{
        margin-top: 3rem !important;
    }

    #img{
        width: 60%;
    }
    #img_background{
        width: 96%;
    }
    

</style>
<body class="">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center" id="card">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block">
                                <img id="img_background"src="https://i.pinimg.com/474x/e0/85/57/e085579123da87b517d28e5eb7d026ca.jpg"/>

                            </div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <img id="img"src="https://americana.edu.co/barranquilla/wp-content/uploads/2018/10/Logo-desktop.png"/>

                                    </div>
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4 mt-3">¡Bienvenido!</h1>
                                    </div>
                                    <form class="user" runat="server">
                                        <div class="form-group">
                                            <asp:TextBox ID="txtUsername" 
                                                placeholder="Usuario"
                                                class="form-control form-control-user" runat="server" 
                                                ValidationGroup="login"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rqvUser" runat="server" 
                                                    style="color: darkred; font-weight: 600"
                                                    ErrorMessage="El usuario es requerido" 
                                                    ControlToValidate="txtUsername" ValidationGroup="login">El usuario es requerido
                                            </asp:RequiredFieldValidator>

                                        </div>
                                        <div class="form-group">
                                            <asp:TextBox ID="txtPassword" 
                                                placeholder="Contraseña"
                                                TextMode="Password" class="form-control form-control-user" 
                                                runat="server" ValidationGroup="login"></asp:TextBox>
                                           <asp:RequiredFieldValidator ID="rqvPassword" runat="server" 
                                                    style="color: darkred; font-weight: 600"
                                                    ErrorMessage="La contraseña es requerida" 
                                                    ControlToValidate="txtPassword" ValidationGroup="login">La contraseña es requerida
                                            </asp:RequiredFieldValidator>
                                        </div>

                                       <asp:LinkButton ID="btnConsultar" runat="server" 
                                         style="background-color: #0d6efd !important; color: white !important;"
                                         class="btn btn-primary btn-user btn-block text-white" OnClick="btnConsultar_Click" Width="349px" ValidationGroup="login">
                                           <i class="bi bi-box-arrow-in-right"></i> Ingresar
                                      </asp:LinkButton>

                                      <div class="form-group">
                                           <h4 style="text-align: center; color: #2f5a78;">
                                               <asp:Label ID="lblMensaje" runat="server"></asp:Label></h4>
                                       </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>