﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="citasPaciente.aspx.cs" Inherits="PruebaBdAspNet.citasPaciente" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Citas paciente</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!-- Custom styles for this page -->
    <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>
<form runat="server">
<body id="page-top">

        <div class="container">
            <div class="modal fade" id="loginModal">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" style="font-weight: 700">Asignación de nueva cita</h4>
                            <button type="button" class="close" data-dismiss="modal">
                                ×
                            </button>
                        </div>

                            <div class="modal-body">
                            
                                <div class="row" style="padding: 5%">

                                       <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Fecha de la cita</label>
                                                <input class="form-control" type="date" runat="server"
                                                       placeholder="Fecha" id="inputDate" ValidationGroup="save"/>
                                                <asp:RequiredFieldValidator ID="rqvDate" runat="server" 
                                                    style="color: darkred; font-weight: 600"
                                                    ErrorMessage="La fecha es requerida" 
                                                    ControlToValidate="inputDate" ValidationGroup="save">La fecha es requerida</asp:RequiredFieldValidator>
                                             </div>
                                       </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                 <label for="exampleInputEmail1">Hora de la cita</label>

                                                   <asp:DropDownList class="form-control"
                                                       ValidationGroup="save"
                                                       ID="hourValue"
                                                       runat="server">
                                                       <asp:ListItem value=""></asp:ListItem>
                                                        <asp:ListItem>08:20</asp:ListItem>
                                                        <asp:ListItem>08:40</asp:ListItem>
                                                        <asp:ListItem>09:00</asp:ListItem>
                                                        <asp:ListItem>09:20</asp:ListItem>
                                                        <asp:ListItem>09:40</asp:ListItem>
                                                        <asp:ListItem>10:00</asp:ListItem>
                                                        <asp:ListItem>10:20</asp:ListItem>
                                                        <asp:ListItem>10:40</asp:ListItem>
                                                        <asp:ListItem>11:00</asp:ListItem>
                                                        <asp:ListItem>11:20</asp:ListItem>
                                                        <asp:ListItem>11:40</asp:ListItem>
                                                    </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="rqvHora" runat="server" 
                                                    style="color: darkred; font-weight: 600"
                                                    ErrorMessage="La hora es requerida" 
                                                    ControlToValidate="hourValue" ValidationGroup="save">La hora es requerida</asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                 <label for="exampleInputEmail1">Médico</label>

                                               <asp:DropDownList class="form-control" ID="selectMedicoValue" 
                                                   ValidationGroup="save" Enabled="true" runat="server">
                                                    <asp:ListItem value=""></asp:ListItem>
                                                    <asp:ListItem></asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="rqvMedico" runat="server" 
                                                    style="color: darkred; font-weight: 600"
                                                    ErrorMessage="El medico es requerida" 
                                                    ControlToValidate="selectMedicoValue" ValidationGroup="save">El medico es requerida</asp:RequiredFieldValidator>

<%--                                                 <select class="form-control" id="selectMedico">
                                                       <% foreach (var medico in medicosList) {%>

                                                              <option value="<%=medico.id%>"> <%=medico.nombres%> <%=medico.apellidos%></option>
                                           
                                                         <% } %>
                                                 </select>--%>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                             <div class="form-group">
                                                <label for="exampleInputEmail1">Lugar de la cita</label>
                                                <input class="form-control" placeholder="CUA Enfermeria" type="text" ID="txtLugar" runat="server" readonly/>
                                            </div>
                                        </div>
                                </div>

                         
                        </div>

                            <div class="modal-footer">
                            <button type="button" id="btnHideModal" class="btn btn-secondary button">
                                Cancelar
                            </button>
                           <asp:LinkButton ID="btnSave" runat="server" 
                             style="color: white !important;"
                             class="btn btn-primary text-white" OnClick="btnSave_Click" ValidationGroup="save">
                               Guardar cita
                          </asp:LinkButton>

                        </div>
                        
                    </div>
                </div>
            </div>
    </div>

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon">
                    <i class="fas fa-hospital"></i>

                </div>
                <div class="sidebar-brand-text mx-3">AMERICACLINIC </div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item">
                <a class="nav-link" href="dashboard.aspx">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Inicio</span></a>
            </li>

   

            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Informacion
            </div>

            <!-- Nav Item - Pages Collapse Menu -->
        

            <!-- Nav Item - Tables -->
            <li class="nav-item active">
                <a class="nav-link" href="citasPaciente.aspx">
                    <i class="far fa-calendar"></i>
                    <span>Historico de citas</span></a>
            </li>

            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

        </ul>
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <form class="form-inline">
                        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                    </form>

                    <!-- Topbar Search -->
     

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                        <li class="nav-item dropdown no-arrow d-sm-none">
                            <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-search fa-fw">
                            </i>
                            </a>
                            <!-- Dropdown - Messages -->
                            <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                                aria-labelledby="searchDropdown">
                                <form class="form-inline mr-auto w-100 navbar-search">
                                    <div class="input-group">
                                        <input type="text" class="form-control bg-light border-0 small"
                                            placeholder="Search for..." aria-label="Search"
                                            aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="button">
                                                <i class="fas fa-search fa-sm"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </li>

             

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><%=usuario.nombres + " " + usuario.apellidos%></span>
                                <img class="img-profile rounded-circle"
                                    src="img/undraw_profile.svg">
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Perfil
                                </a>
                            
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Salir
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Tus citas</h1>
                    <p class="mb-4">Tu resumen de citas actualizado hasta la fecha <%=DateTime.Now.ToString("yyyy-MM-dd HH:MM") %></p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Historico de citas del paciente <%=usuario.nombres + " " + usuario.apellidos%> </h6>
                            
                        </div>
                        <div class="card-body">

                            <div class="row">

                                <div class="col-12 mb-3">



                                      <buttom 
                                           id="btnclick"
                                          class="btn btn-success btn-icon-split float-right">
                                        <span class="icon text-white-50">
                                            <i class="far fa-calendar-plus"></i>
                                        </span>
                                        <span class="text">Asignar nueva cita</span>
                                    </buttom>
                                    </div>

                                     

                            </div>


                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>                                    
                                            <th>Medico</th>
                                            <th>Tipo</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Lugar</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                           <% foreach (var cita in citasList) {%>

                                                <tr>                                         
                                                <td><%=cita.nombresMedico + " " + cita.apellidosMedico  %></td>
                                                <td><%=cita.tipoMedico %></td>
                                                <td><%=cita.fecha %></td>
                                                <td><%=cita.hora %></td>
                                                <td><%=cita.lugar %></td>   
                                                <td>

                                                   <% if(cita.estado == "0") { %>
                                                   <span class="mr-2"> Pendiente </span>   <i class="far fa-calendar text-warning float-right"></i>
                                                   <% } %>

                                                   <% if(cita.estado == "1") { %>
                                                   <span class="mr-2"> Completada </span>   <i class="far fa-calendar-check text-success float-right"></i>
                                                   <% } %>

                                                   <% if(cita.estado == "2") { %>
                                                   <span class="mr-2"> Cancelada </span>   <i class="far fa-calendar-times text-danger float-right"></i>
                                                   <% } %>

                                                </td>  
                                               </tr>

                                           
                                             <% } %>
                                       
                                    </tbody>
                                </table>
                            </div>

                        <% if(alert == true) { %>
                            <div class="alert alert-success" role="alert">
                                Cita creada con éxito!
                            </div>
                        <% } %>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; AMERICACLINIC 2022</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">

                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Seguro que quieres salir?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                        <asp:LinkButton ID="salirBtn" runat="server" class="btn btn-primary" 
                            OnClick="logOut" Width="213px">Salir</asp:LinkButton>
                    </div>
                </div>

        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="js/demo/datatables-demo.js"></script>

    <script type="text/javascript">

        $("#btnclick").click(function () {
            $("#loginModal").modal('show');
        });

        $("#btnHideModal").click(function () {
            $("#loginModal").modal('hide');
        });
    </script>

</body>
</form>
</html>