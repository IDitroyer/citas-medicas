﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Dao;
using Business;

namespace PruebaBdAspNet
{
    public partial class CitasMedico : System.Web.UI.Page
    {
        CitaDao citaDao = new CitaDao();
        UsuarioDao usuarioDao = new UsuarioDao();
        public List<Cita> citasList = new List<Cita>();
        public Usuario usuario = new Usuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Auth();
        }

        protected void Auth()
        {
            if (Session["idUsuario"] == null)
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                this.Consultar();
            }
        }

        private void Consultar()
        {

            usuario = usuarioDao.findById(int.Parse(Session["idUsuario"].ToString()));
            citasList = citaDao.findCitasByUsuario(usuario.id, 1);

            //LiteralUsuarios.Text = html;
        }


        protected void logOut(object sender, EventArgs e)
        {
            Session["idUsuario"] = null;
            Response.Redirect("login.aspx");
        }
    }
}