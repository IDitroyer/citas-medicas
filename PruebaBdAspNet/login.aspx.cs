﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data;
using Dao;
using Business;

namespace PruebaBdAspNet
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConsultar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            string vUsername, vPassword;
            vUsername = txtUsername.Text;
            vPassword = txtPassword.Text;
            var usuario = new Usuario();
            UsuarioDao usuarioDao = new UsuarioDao();

            usuario = usuarioDao.PermitirAcceso(vUsername, vPassword);
            if (usuario != null)
            {
                System.Diagnostics.Debug.WriteLine("Debug message" + usuario.id);
                Session["idUsuario"] = usuario.id;
                FormsAuthentication.RedirectFromLoginPage(vUsername, true);
                Response.Redirect("dashboard.aspx");
            }
            else
            {
                lblMensaje.Text = "No se ha encontrado un usuario, intente con otra combinación";
            }
        }

    }
}