﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class Cita
    {

        public int id { get; set; }
        public int idPaciente { get; set; }
        public int idMedico  { get; set; }
        public string nombresPaciente { get; set; }
        public string apellidosPaciente { get; set; }
        public string nombresMedico { get; set; }
        public string apellidosMedico { get; set; }
        public string tipoMedico { get; set; }
        public string lugar { get; set; }
        public string fecha { get; set; }
        public string hora { get; set; }
        public string estado { get; set; } 


    }
}
