﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    public class Usuario
    {

        public int id { get; set; }
        public string username { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string documento { get; set; }
        public string tipo { get; set; }
        public int edad { get; set; }
        public string estado { get; set; }
        public string password { get; set; }

    }
}
