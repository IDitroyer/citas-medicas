﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using Business;

namespace Dao
{
    public class CitaDao
    {

        bool ejecuto;
        BaseDato bd;


        public CitaDao()
        {
            bd = new BaseDato();
        }

        /// <summary>
        /// Consultar listado de los Citas
        /// </summary>
        /// <returns>Retorna un DataTable</returns>
        /// 
        public List<Cita> findCitasByUsuario(int idUsuario, int type)
        {

            String parameter = type == 0 ? "idUsuario" : "idMedico"; 

            var citas = new List<Cita>();

            bd.Conectar();
            bd.CrearComando($"SELECT * FROM [dbo].[seleccionarCitas] WHERE {parameter} = @id ORDER BY fecha DESC");
            bd.AsignarParametro("@id", SqlDbType.Int, idUsuario);

            try
            {
                DataTable dt = bd.EjecutarDataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dbRow = dt.Rows[i];
                    citas.Add(
                        new Cita()
                        {
                            id = int.Parse(dbRow["idCita"].ToString()),
                            idPaciente = int.Parse(dbRow["idUsuario"].ToString()),
                            idMedico = int.Parse(dbRow["idMedico"].ToString()),
                            nombresPaciente = dbRow["nombres"].ToString(),
                            apellidosPaciente = dbRow["apellidos"].ToString(),
                            nombresMedico = dbRow["nombresMedico"].ToString(),
                            apellidosMedico = dbRow["apellidosMedico"].ToString(),
                            tipoMedico = dbRow["tipoMedico"].ToString(),
                            hora = dbRow["hora"].ToString(),
                            fecha = dbRow["fecha"].ToString(),
                            lugar = dbRow["lugar"].ToString(),
                            estado = dbRow["estado"].ToString(),
                        }
                    );

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                bd.Desconectar();
            }

            return citas;

        }





        public DataTable Consultar()
        {
            DataTable dt;
            bd.Conectar();
            bd.CrearComando("SELECT idCita, idUsuario, nombres, apellidos, tipoMedico, hora, fecha, lugar, estado FROM [dbo].[seleccionarCitas] ORDER BY idCita DESC");
            dt = bd.EjecutarDataTable();
            bd.Desconectar();
            return dt;
        }

        /// <summary>
        /// Consultar Citas medicas por Usuario
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public DataTable ConsultarByUsuario(int idUsuario)
        {
            DataTable dt;
            bd.Conectar();
            bd.CrearComando("SELECT idCita, idUsuario, nombres, apellidos, tipoMedico, hora, fecha, lugar, estado FROM [dbo].[seleccionarCitas] WHERE idUsuario = @ID ORDER BY idCita DESC");
            bd.AsignarParametro("@ID", SqlDbType.BigInt, idUsuario);
            dt = bd.EjecutarDataTable();
            bd.Desconectar();
            return dt;
        }

        public bool Insertar(int FKPACIENTE, int FKMEDICO, string LUGAR, string TIPOCITA, string HORA, string FECHA)
        {
            bd.Conectar();
            bd.CrearComando("EXECUTE [dbo].[guardarCita] @ID, @FKPACIENTE, @FKMEDICO, @LUGAR, @TIPOCITA, @HORA, @FECHA, @ESTADO, @TIPO");
            bd.AsignarParametro("@ID", SqlDbType.BigInt, 0);
            bd.AsignarParametro("@FKPACIENTE", SqlDbType.BigInt, FKPACIENTE);
            bd.AsignarParametro("@FKMEDICO", SqlDbType.BigInt, FKMEDICO);
            bd.AsignarParametro("@LUGAR", SqlDbType.VarChar, LUGAR);
            bd.AsignarParametro("@TIPOCITA", SqlDbType.VarChar, TIPOCITA);
            bd.AsignarParametro("@HORA", SqlDbType.VarChar, HORA);
            bd.AsignarParametro("@FECHA", SqlDbType.DateTime, FECHA);
            bd.AsignarParametro("@ESTADO", SqlDbType.TinyInt, 0);
            bd.AsignarParametro("@TIPO", SqlDbType.TinyInt, 0);
            if (bd.EjecutarConsulta() > 0)
            {
                ejecuto = true;
            }
            else
            {
                ejecuto = false;
            }
            return ejecuto;
        }

        public bool Editar(int id, int FKPACIENTE, int FKMEDICO, string LUGAR, string TIPOCITA, string HORA, DateTime FECHA, int estado)
        {
            bd.Conectar();
            bd.CrearComando("EXECUTE [dbo].[guardarCita] @ID, @FKPACIENTE, @FKMEDICO, @LUGAR, @TIPOCITA, @HORA, @FECHA, @ESTADO, @TIPO");
            bd.AsignarParametro("@ID", SqlDbType.BigInt, id);
            bd.AsignarParametro("@FKPACIENTE", SqlDbType.BigInt, FKPACIENTE);
            bd.AsignarParametro("@FKMEDICO", SqlDbType.BigInt, FKMEDICO);
            bd.AsignarParametro("@LUGAR", SqlDbType.VarChar, LUGAR);
            bd.AsignarParametro("@TIPOCITA", SqlDbType.VarChar, TIPOCITA);
            bd.AsignarParametro("@HORA", SqlDbType.VarChar, HORA);
            bd.AsignarParametro("@FECHA", SqlDbType.DateTime, FECHA);
            bd.AsignarParametro("@ESTADO", SqlDbType.TinyInt, estado);
            bd.AsignarParametro("@TIPO", SqlDbType.TinyInt, 1);
            if (bd.EjecutarConsulta() > 0)
            {
                ejecuto = true;
            }
            else
            {
                ejecuto = false;
            }
            return ejecuto;
        }
    }
}
