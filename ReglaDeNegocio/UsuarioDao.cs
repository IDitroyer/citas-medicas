﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using AccesoDatos;
using Business;

namespace Dao
{
    public class UsuarioDao
    {

        bool ejecuto;
        BaseDato bd;


        public UsuarioDao()
        {
            bd = new BaseDato();
        }


        /// <summary>
        /// Consultar listado de los Usuarios
        /// </summary>
        /// <returns>Retorna una lista de usuarios</returns>
        public List<Usuario> findAll()
        {
            var usuarios = new List<Usuario>();

            bd.Conectar();
            bd.CrearComando("SELECT * FROM [dbo].[seleccionarUsuarios] ORDER BY tipo");

            try
            {
                DataTable dt = bd.EjecutarDataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dbRow = dt.Rows[i];
                    usuarios.Add(
                        new Usuario()
                        {
                            id = int.Parse(dbRow["idUsuario"].ToString()),
                            username = dbRow["username"].ToString(),
                            nombres = dbRow["nombres"].ToString(),
                            apellidos = dbRow["apellidos"].ToString(),
                            documento = dbRow["documento"].ToString(),
                            edad = int.Parse(dbRow["edad"].ToString()),
                            estado = dbRow["estado"].ToString(),
                            tipo = dbRow["tipo"].ToString()

                        }
                    );

                }
            } catch (Exception ex)
            {
                throw new Exception(ex.Message);
            } finally
            {
                bd.Desconectar();
            }
  
            return usuarios;
        }

        public Usuario findById(int id)
        {
            var usuario = new Usuario();

            bd.Conectar();
            bd.CrearComando("SELECT * FROM [dbo].[seleccionarUsuarios] WHERE idUsuario = @id");
            bd.AsignarParametro("@id", SqlDbType.Int, id);

            try
            {
                DataTable dt = bd.EjecutarDataTable();

                if (dt.Rows.Count > 0)
                {
                     DataRow dbRow = dt.Rows[0];

                    return new Usuario()
                    {
                        id = int.Parse(dbRow["idUsuario"].ToString()),
                        username = dbRow["username"].ToString(),
                        nombres = dbRow["nombres"].ToString(),
                        apellidos = dbRow["apellidos"].ToString(),
                        documento = dbRow["documento"].ToString(),
                        edad = int.Parse(dbRow["edad"].ToString()),
                        estado = dbRow["estado"].ToString(),
                        tipo = dbRow["tipo"].ToString()
                    };


                      
                } else
                {
                    throw new Exception("No se econtro el usuario con el id " + id);
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                bd.Desconectar();
            }

        }


        /// <summary>
        /// Consultar listado de los Usuarios medicos
        /// </summary>
        /// <returns>Retorna una lista de usuarios medicos</returns>
        public List<Usuario> findAllMedicos()
        {
            var usuarios = new List<Usuario>();

            bd.Conectar();
            bd.CrearComando("SELECT * FROM [dbo].[seleccionarUsuarios] WHERE tipo = @tipo ORDER BY tipo");
            bd.AsignarParametro("@tipo", SqlDbType.VarChar, "Medico");

            try
            {
                DataTable dt = bd.EjecutarDataTable();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dbRow = dt.Rows[i];
                    usuarios.Add(
                        new Usuario()
                        {
                            id = int.Parse(dbRow["idUsuario"].ToString()),
                            username = dbRow["username"].ToString(),
                            nombres = dbRow["nombres"].ToString(),
                            apellidos = dbRow["apellidos"].ToString(),
                            documento = dbRow["documento"].ToString(),
                            edad = int.Parse(dbRow["edad"].ToString()),
                            estado = dbRow["estado"].ToString(),
                            tipo = dbRow["tipo"].ToString()

                        }
                    );

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                bd.Desconectar();
            }

            return usuarios;
        }





        public DataTable ConsultarUsuariosByTipo(string tipo)
        {
            DataTable dt;
            bd.Conectar();
            bd.CrearComando("SELECT idUsuario, username, nombres, apellidos, documento, tipo, estado FROM [dbo].[seleccionarUsuarios] WHERE tipo = @TIPO");
            bd.AsignarParametro("@TIPO", SqlDbType.VarChar, tipo);
            dt = bd.EjecutarDataTable();
            bd.Desconectar();
            return dt;
        }

        public Usuario PermitirAcceso(string username, string password)
        {
            var usuario = new Usuario();
            DataTable dt;
            bool permitir = false;
            bd.Conectar(); 
            bd.CrearComando("SELECT * FROM [dbo].[usuarios] WHERE username = @USERNAME and password = @PASSWORD");
            bd.AsignarParametro("@USERNAME", SqlDbType.VarChar, username);
            bd.AsignarParametro("@PASSWORD", SqlDbType.VarChar, password);
            dt = bd.EjecutarDataTable();
            bd.Desconectar();

            if (dt.Rows.Count>0)
            {
                permitir = true;
                DataRow dbRow = dt.Rows[0];
                usuario = new Usuario()
                {
                    id = int.Parse(dbRow["idUsuario"].ToString()),
                    username = dbRow["username"].ToString(),
                    nombres = dbRow["nombres"].ToString(),
                    apellidos = dbRow["apellidos"].ToString(),
                    documento = dbRow["documento"].ToString(),
                    edad = int.Parse(dbRow["edad"].ToString()),
                    estado = dbRow["estado"].ToString(),
                    tipo = dbRow["fkTipo"].ToString()

                };
                return usuario;
            }
            else
            {
                return null;
            }


        }



   
    }
}
